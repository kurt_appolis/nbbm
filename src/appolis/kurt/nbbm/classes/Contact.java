package appolis.kurt.nbbm.classes;

import appolis.kurt.nbbm.R;

public class Contact {
	private String name = "Contact Name";
	private int icon_picture = R.drawable.default_icon;
	private boolean message = false;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIcon_picture() {
		return icon_picture;
	}

	public void setIcon_picture(int icon_picture) {
		this.icon_picture = icon_picture;
	}

	public Contact(String name, int icon, boolean message) {
		this.name = name;
		this.icon_picture = icon;
		this.message = message;
	}

	public boolean isMessage() {
		return message;
	}

	public void setMessage(boolean message) {
		this.message = message;
	}
}
