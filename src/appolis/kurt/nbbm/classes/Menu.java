package appolis.kurt.nbbm.classes;

import appolis.kurt.nbbm.R;

public class Menu {
	private String name = "Contact Name";
	private String sub_text = "Default text";
	private int icon_picture = R.drawable.default_icon;
	private boolean message = false;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIcon_picture() {
		return icon_picture;
	}

	public void setIcon_picture(int icon_picture) {
		this.icon_picture = icon_picture;
	}

	public Menu(String name, String sub_text, int icon, boolean message) {
		this.name = name;
		this.sub_text = sub_text;
		this.icon_picture = icon;
		this.message = message;
	}

	public String getSub_text() {
		return sub_text;
	}

	public void setSub_text(String sub_text) {
		this.sub_text = sub_text;
	}

	public boolean isMessage() {
		return message;
	}

	public void setMessage(boolean message) {
		this.message = message;
	}
}
