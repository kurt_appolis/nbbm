package appolis.kurt.nbbm.fragment;

import java.util.ArrayList;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import appolis.kurt.nbbm.R;
import appolis.kurt.nbbm.adapter.GridViewCustomAdapter;
import appolis.kurt.nbbm.classes.Contact;

public class LandingPage extends Fragment {
	private View view = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.view = inflater.inflate(R.layout.landing_page, container, false);

		setupView();
		return this.view;

	}

	private void setupView() {
		GridView gridView = (GridView) view.findViewById(R.id.gridView1);
		// Create the Custom Adapter Object

		ArrayList<Contact> contacts = new ArrayList<Contact>();
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, false));
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, true));
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, false));

		contacts.add(new Contact("Contact Name", R.drawable.default_icon, false));
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, false));
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, true));

		contacts.add(new Contact("Contact Name", R.drawable.default_icon, true));
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, false));
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, false));

		contacts.add(new Contact("Contact Name", R.drawable.default_icon, true));
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, false));
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, false));

		contacts.add(new Contact("Contact Name", R.drawable.default_icon, true));
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, false));
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, false));

		contacts.add(new Contact("Contact Name", R.drawable.default_icon, true));
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, false));
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, false));

		contacts.add(new Contact("Contact Name", R.drawable.default_icon, true));
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, false));
		contacts.add(new Contact("Contact Name", R.drawable.default_icon, false));

		GridViewCustomAdapter grisViewCustomeAdapter = new GridViewCustomAdapter(
				view.getContext(), contacts);
		// Set the Adapter to GridView
		gridView.setAdapter(grisViewCustomeAdapter);

		// Handling touch/click Event on GridView Item
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long arg3) {
				String selectedItem;
				if (position % 2 == 0)
					selectedItem = "Contact Name";
				else
					selectedItem = "Contact Name";
				Toast.makeText(view.getContext(),
						"Selected Item: " + selectedItem, Toast.LENGTH_SHORT)
						.show();

			}
		});

		TextView textView2 = (TextView) view.findViewById(R.id.textView2);
		textView2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
			}

		});

		TextView landing_page_contact_count = (TextView) view
				.findViewById(R.id.landing_page_contact_count);
		landing_page_contact_count.setText("" + contacts.size());

	}

}
