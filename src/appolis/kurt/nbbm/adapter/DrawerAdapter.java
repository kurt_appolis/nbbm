package appolis.kurt.nbbm.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import appolis.kurt.nbbm.R;

public class DrawerAdapter extends ArrayAdapter<appolis.kurt.nbbm.classes.Menu> {

	private Context context = null;
	private ArrayList<appolis.kurt.nbbm.classes.Menu> menus = new ArrayList<appolis.kurt.nbbm.classes.Menu>();

	public DrawerAdapter(Context context,
			ArrayList<appolis.kurt.nbbm.classes.Menu> menus) {
		super(context, R.layout.drawer_item, menus);
		this.context = context;
		this.menus = menus;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		View rootView = arg1;
		if (rootView == null) {
			rootView = LayoutInflater.from(context).inflate(
					R.layout.drawer_item, null);
		}

		ImageView imageView1 = (ImageView) rootView
				.findViewById(R.id.imageView1);
		imageView1.setImageResource(menus.get(arg0).getIcon_picture());

		TextView drawer_item_setting_item = (TextView) rootView
				.findViewById(R.id.drawer_item_setting_item);
		drawer_item_setting_item.setText(menus.get(arg0).getName());

		TextView textView2 = (TextView) rootView.findViewById(R.id.textView2);
		String sub_text = menus.get(arg0).getSub_text();
		if (!sub_text.equalsIgnoreCase("")) {
			textView2.setText(sub_text);
			textView2.setVisibility(View.VISIBLE);
		} else {
			textView2.setVisibility(View.GONE);
		}

		return rootView;
	}

}
