package appolis.kurt.nbbm.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import appolis.kurt.nbbm.R;
import appolis.kurt.nbbm.classes.Contact;

public class GridViewCustomAdapter extends BaseAdapter {
	Context context;
	ArrayList<appolis.kurt.nbbm.classes.Contact> contacts = new ArrayList<appolis.kurt.nbbm.classes.Contact>();

	public GridViewCustomAdapter(Context context,
			ArrayList<appolis.kurt.nbbm.classes.Contact> contacts) {
		this.contacts = contacts;
		this.context = context;
	}

	@Override
	public int getCount() {
		return contacts.size();
	}

	@Override
	public Contact getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View gridView;

		if (convertView == null) {

			gridView = new View(context);

			// get layout from mobile.xml
			gridView = inflater.inflate(R.layout.grid_main, null);

			// set value into textview
			TextView textView = (TextView) gridView
					.findViewById(R.id.grid_main_contact_name);
			textView.setText(contacts.get(position).getName());

			// set image based on selected text
			ImageView imageView = (ImageView) gridView
					.findViewById(R.id.imageView1);

			imageView
					.setImageResource(contacts.get(position).getIcon_picture());

			ImageView imageView2 = (ImageView) gridView
					.findViewById(R.id.imageView2);
			if (contacts.get(position).isMessage()) {
				imageView2.setVisibility(View.VISIBLE);
			} else {
				imageView2.setVisibility(View.INVISIBLE);
			}

		} else {
			gridView = (View) convertView;
		}

		return gridView;
	}

}
